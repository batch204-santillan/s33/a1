// activity.js

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response)=> response.json())
.then((json)=> console.log (json));

//  Getting all to-do list items
	fetch('https://jsonplaceholder.typicode.com/todos')
	.then((response)=> response.json())
	.then((json)=> 
			{
				let list = json.map((todo=>{
					return todo.title ;
				}))
				console.log(list);
			}
		);

// Getting a specific to do lost item
	fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then((response)=> response.json())
	.then((json)=> console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

// Creating a todo list item using the POST method
	fetch('https://jsonplaceholder.typicode.com/todos' ,
			{
				method: 'POST',
				headers: 
					{
						'Content-Type': 'application/json'
					},
				body: JSON.stringify({
					title: 'Created a To-Do list item',
					completed: false,
					userId: 1
				})
			})
	.then((response)=> response.json())
	.then((json)=> console.log(json));

// Updating a to-do list item using PUT method
	fetch('https://jsonplaceholder.typicode.com/todos/1', 
			{
				method: 'PUT',
				headers: 
					{
						'Content-Type': 'application/json'
					},
				body: JSON.stringify ({
					title: 'Updated To D list item',
					description: 'To update the my to-do list with a different data structure',
					status: 'Pending',
					dateCompleted: 'Pending',
					userID: 1

				})
			})
	.then((response)=> response.json())
	.then((json)=> console.log(json));

// Updating using the PATCH method
	fetch('https://jsonplaceholder.typicode.com/todos/1',
			{
				method: 'PATCH',
				headers: 
					{
						'Content-Type': 'application/json'
					},
				body: JSON.stringify ({
					status: 'Complete',
					dateCompleted: '30/09/2022' 
				})
			})
	.then((response)=> response.json())
	.then((json)=> console.log(json));

// Deleting a to-do list item
	fetch('https://jsonplaceholder.typicode.com/todos/1' ,	{
			method: 'DELETE'
	});